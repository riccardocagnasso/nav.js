# NAV.js

NAV class connect via autobahn/crossbar to a NAV proxy.
You can use this to send and receive all UMP messages and
events in a NAV network.


## Examples

To test the examples, run a webserver from the main directory, like

~~~~
(nav) [phas@vostok nav.js]$ python -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
~~~~
