class TouchBar {
    constructor(
            bar,
            callback,
            start_callback=null,
            end_callback=null, 
            polling_interval_msec=250,
            touch_callback=null,
            vertical=false
    ) {
        this.bar = bar;
        this.callback = callback;
        this.start_callback = start_callback;
        this.end_callback = end_callback;
        this.touch_callback = touch_callback;
        this.vertical = vertical;

        this.bar.on('touchmove', event => { this.on_touch_move(event) });
        this.bar.on('touchstart', event => { this.on_touch_start(event) });
        this.bar.on('touchend', event => { this.on_touch_end(event) })

        if(this.vertical){
            this.barpos = this.bar.offset().top;
            this.barw = this.bar.width();
        }else{
            this.barpos = this.bar.offset().left;
            this.barw = this.bar.width();
        }

        this.interval = setInterval(
            () => this.on_interval_trigger(),
            polling_interval_msec
        );
    }

    on_touch_move(event){
        var touchpos;
        var part;
        if(this.vertical){
            touchpos = event.touches[0].pageY;
            part = (1- (touchpos - this.barpos) / this.barw)
        }else{
            touchpos = event.touches[0].pageX;
            part = (touchpos - this.barpos) / this.barw
        }

        let completion = Math.max(
            0, Math.min(part, 1)
        )

        this.completion = completion;
        this.dirty = true;

        if(this.touch_callback){
            this.touch_callback(completion);
        }
    }

    on_touch_start(event){
        if(this.start_callback){
            this.start_callback(event);
        }
    }

    on_touch_end(event){
        if(this.end_callback){
            this.end_callback(event);
        }
    }

    on_interval_trigger(){
        if(this.dirty){
            if(this.callback){
                this.callback(
                    this.completion,
                    this.completion * this.bar.attr('max')
                );
            }
            this.dirty = false;
        }
    }
}