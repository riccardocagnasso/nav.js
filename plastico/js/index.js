var nav;
var status_interval;

$(function(){
    let url = new URL(window.location);
    let hostname = url.hostname;

    var init_nav;

    var close_callback = () => {
        //console.log("CLOSE CALLBACK");
        //init_nav();
        window.location.reload();
    }

    init_nav = () => {
        console.log("INIT NAV");
        nav = new NAV(
            'plastico_touch',
            url=`ws://${hostname}:8080/ws`,
            realm="realm1",
            close_callback=close_callback
        );
    }

    init_nav();

    nav.send_command('plastico_player', 'get_status');

    nav.on_response = response => {
        if(response.subject == 'get_status'){
            console.log("STATUS", response);
            if(response.payload.state == "GST_STATE_PLAYING"){
                show_pause();
            }else if(response.payload.state == "GST_STATE_PAUSED"){
                show_resume();
            }

            $('progress#video_progress')
                .attr('max', response.payload.duration)
                .attr('value', response.payload.current_position);
        }else if(response.subject == 'get_playlist'){
            for (var video of response.payload.videos) {
                add_video_element(video.name);
            }
            clearInterval(status_interval);
            new Siema();
        }
    }

    setInterval(function(){
        nav.send_command('plastico_player', 'get_status');
    }, 100);

    status_interval = setInterval(function(){
        nav.send_command('plastico_player', 'get_playlist');
    }, 500);

    new TouchBar(
        $('progress#video_progress'),
        (rel_pos, abs_pos) => {
            nav.send_command(
                'plastico_player',
                'seek',
                {
                    position: abs_pos,
                    flags: [
                        'FLUSH',
                        'ACCURATE',
                        'TRICKMODE'
                    ],
                    pause: false
                });
        },
        () => pause(),
        () => resume(),
        350
    )

        new TouchBar(
        $('progress#volume_progress'),
        (rel_pos, abs_pos) => {
            console.log("LEVEL", rel_pos);
            nav.send_command(
                'plastico_player',
                'volume',
                {
                    level: rel_pos
                });
        },
        () => console.log("START VOLUME"),
        () => console.log("END VOLUME"),
        350,
        (completion) => $('progress#volume_progress').val(completion),
        vertical=true
    )
})

function add_video_element(name){
    var element = `
        <div class="video" onClick="sobstitute_video(\'${name}\')">
            <img src="img/${name}.jpg">
            <div class="video_float">
                <div class="play_button_container">
                    <i class="play_button fa fa-play"></i>
                </div>
            </div>
        </div>
    `;

    $('div#videos').append(element);
}

function jump(seconds){
    nav.send_command('plastico_player', 'seek', {delta: seconds});
}

function show_resume(){
    $('#resume').show();
    $('#pause').hide();
}

function show_pause(){
    $('#resume').hide();
    $('#pause').show();
}

function pause(){
    nav.send_command('plastico_player', 'pause')
}

function resume(){
    nav.send_command('plastico_player', 'resume')
}

function sobstitute_video(name){
    nav.send_command('plastico_player', 'sobstitute_video', {
        name: name
    });
}