/*
* Copyright © 2019 Riccardo Cagnasso <riccardo@phascode.org>
*
* This program is free software. It comes without any warranty, to
* the extent permitted by applicable law. You can redistribute it
* and/or modify it under the terms of the Do What The Fuck You Want
* To Public License, Version 2, as published by Sam Hocevar. See
* http://www.wtfpl.net/ for more details.
*/


class NAV {
    /*
     * NAV class connect via autobahn/crossbar to a NAV proxy.
     * You can use this to send and receive all UMP messages and
     * events in a NAV network.
     *
     * The name parameters is used to identify this application
     * in the NAV network. Names must be unique. When a predictable
     * name is not needed (i.e. the application doesn't to receive
     * commands) it's suggested to randomly generate a name with uuid1
     * as is standard in the rest of the NAV network. This is not provided
     * as a functionality from this library since UUID is not part of the
     * javascript standard library.
     */
    constructor(
            name,
            url="ws://localhost:8080/ws",
            realm="realm1",
            close_callback=()=>{}
    ){
        this.name = name;
        this.url = url;
        this.realm = realm;
        this.channel = 'org.nav.modules.' + this.name;

        this.close_callback = close_callback;

        this.pre_init_queue = [];

        this.connection = new autobahn.Connection({
           url: this.url,
           realm: this.realm
        });

        this.connection.onopen = (session => this.on_open(session))
        this.connection.open()
    }

    on_open(session){
        this.session_ = session;
        this.session_.subscribe(
            this.channel,
            (message => this.__on_message(message))
        );
        //this.session_.publish('org.nav.announce', [this.name]);
        this.announce();

    }

    announce(){
        if(!this.session){
            this.session_.publish('org.nav.announce', [this.name]);
            setTimeout( x => this.announce(), 1000);
        }
    }

    after_init(){
        this.session = this.session_

        for (let v of this.pre_init_queue) {
            this.publish(v[0], v[1]);
        }
    }

    __on_message(message){
        if(message.length > 0){
            message = message[0]
        }else{
            return
        }

        if (message.type == 'control' &&
            message.subject == 'init'){
                this.after_init();
        }else{
            this.on_message(message);
        }
    }

    on_message(message){
        /*
         * This is a callback for every message received.
         * You can redefine this to handle message or you
         * can use the by-type functions.
         */
        switch(message.type){
            case 'command':
                this.on_command(message);
                break
            case 'control':
                this.on_control(message);
                break;
            case 'response':
                this.on_response(message);
                break;
            case 'event':
                this.on_event(message);
        }
    }

    on_command(command){}
    on_control(contro){}
    on_response(response){}
    on_event(event){}

    publish(channel, content){
        if(this.session){
            try {
                this.session.publish(channel, content);
            }catch(err){
                if(err == "session not open"){
                    this.connection.close();
                    this.close_callback();
                }else{
                    throw err;
                }
            }
        }else{
            this.pre_init_queue.push([channel, content]);
        }
    }

    send_command(target, subject, payload={}){
        /*
         * Sends a command to a target in the NAV network
         */
        this.publish(this.channel, [{
            type: 'command',
            target: target,
            subject: subject,
            payload: payload
        }])
    }

    send_event(channel, subject, payload={}){
        /*
         * Sends a event in the NAV network
         */
        this.publish(this.channel, [{
            type: 'event',
            subject: subject,
            channel: channel,
            payload: payload
        }])
    }

    send_control(subject, payload={}, target=null){
        /*
         * Sends a control message in the NAV network
         */
        this.publish(this.channel, [{
            type: 'control',
            subject: subject,
            payload: payload,
            target: target,
        }])
    }

    subscribe_channel(channel, subject=null, sender=null, local_only=false){
        /*
         * Subscribe to an event channe in the NAV network
         */
        this.send_control('subscribe_channel', {
            channel: channel,
            subject: subject,
            sender: sender,
            local_only: local_only
        });
    }
}
